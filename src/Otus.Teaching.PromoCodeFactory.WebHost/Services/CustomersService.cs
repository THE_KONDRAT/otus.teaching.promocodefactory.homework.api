﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Grpc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using Google.Protobuf;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerShortResponseList> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersShortResponse = customers.Select(x => new CustomerShortResponse()
            {
                Id = GetCustomerIdFromGuid(x.Id),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return new CustomerShortResponseList { Customers = { customersShortResponse } };
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerId request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(GetGuidFromCustomerId(request));
            var response = new CustomerResponse
            {
                Id = GetCustomerIdFromGuid(customer.Id),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = { customer.Preferences.Select(q => new PreferenceResponse { Id = GetPreferenceIdFromGuid(q.Preference.Id), Name = q.Preference.Name }) },
            };
            return response;
        }

        public override async Task<CustomerId> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(q => GetGuidFromPreferenceId(q)).ToList());
            var customer = new Customer
            {
                Id = Guid.NewGuid(),

                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            await _customerRepository.AddAsync(customer);
            return GetCustomerIdFromGuid(customer.Id);
        }

        public override async Task<Empty> EditCustomers(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await GetUserById(GetGuidFromCustomerId(request.Id));
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(q => GetGuidFromPreferenceId(q)).ToList());
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            await _customerRepository.UpdateAsync(customer);
            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
            var customer = await GetUserById(GetGuidFromCustomerId(request));
            await _customerRepository.DeleteAsync(customer);
            return new Empty();
        }
        
        private CustomerId GetCustomerIdFromGuid(Guid id) => new CustomerId { Value = GetByteStringFromGuid(id) };
        private Guid GetGuidFromCustomerId(CustomerId id) => GetGuidFromByteString(id.Value);
        private PreferenceId GetPreferenceIdFromGuid(Guid id) => new PreferenceId { Value = GetByteStringFromGuid(id) };
        private Guid GetGuidFromPreferenceId(PreferenceId id) => GetGuidFromByteString(id.Value);
        private PromoCodeId GetPromoCodeIdFromGuid(Guid id) => new PromoCodeId { Value = GetByteStringFromGuid(id) };
        private Guid GetGuidFromPromoCodeId(PromoCodeId id) => GetGuidFromByteString(id.Value);

        private ByteString GetByteStringFromGuid(Guid id) => ByteString.CopyFromUtf8(id.ToString());
        private Guid GetGuidFromByteString(ByteString id) => Guid.Parse(id.ToStringUtf8());

        private async Task<Customer> GetUserById(Guid id) =>
            await _customerRepository.GetByIdAsync(id) ?? throw new Exception($"Customer with id=[{id}] not found");
    }
}
